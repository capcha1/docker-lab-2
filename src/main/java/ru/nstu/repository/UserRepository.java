package ru.nstu.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nstu.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
