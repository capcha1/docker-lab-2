package ru.nstu.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nstu.entity.Bot;

@Repository
public interface BotRepository extends CrudRepository<Bot, Long> {
}
