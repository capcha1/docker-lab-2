package ru.nstu.api;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.nstu.entity.User;
import ru.nstu.services.UserService;

import java.util.Optional;

@Controller
@AllArgsConstructor
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    @GetMapping
    public String redirect() {
        return "redirect:/users/index";
    }

    @GetMapping("/addUser")
    public String showSignUpForm(User user) {
        return "add-user";
    }

    @PostMapping("/addUser")
    public String createUser(
            User user, BindingResult result, Model model) {

        if (result.hasErrors()) {
            return "add-user";
        }

        userService.createUser(user);

        model.addAttribute("users", userService.getAllUsers());

        return "redirect:/users/index";
    }

    @GetMapping("/index")
    public String showIndex(User user, Model model) {
        model.addAttribute("users", userService.getAllUsers());
        return "index";
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") String id, Model model) {
        User user = userService.getUserById(Long.parseLong(id))
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

        userService.deleteUser(user);

        model.addAttribute("users", userService.getAllUsers());

        return "redirect:/users/index";
    }
}
