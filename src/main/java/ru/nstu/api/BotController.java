package ru.nstu.api;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.nstu.entity.Bot;
import ru.nstu.entity.User;
import ru.nstu.services.BotService;
import ru.nstu.services.UserService;

import java.util.List;
import java.util.Optional;

@Controller
@AllArgsConstructor
@RequestMapping("/bots")
public class BotController {
    private final BotService botService;
    private final UserService userService;

    @GetMapping("/addBot/{userId}")
    public String showSignUpForm(@PathVariable("userId") String userId, Bot bot) {
        return "add-bot";
    }

    @PostMapping("/addBot/{userId}")
    public String createBot(@PathVariable("userId") String userId,
            Bot bot, BindingResult result, Model model) {

        if (result.hasErrors()) {
            return "add-bot";
        }

        User user = userService.getUserById(Long.parseLong(userId)).orElse(new User());

        List<Bot> botList = user.getBotList();

        bot.setStatus("OK");
        bot.setToken("56ac4123cf431a");
        bot.setMode("auto");
        bot.setMood("tired");

        bot.setUser(user);

        botList.add(bot);

        user.setBotList(botList);

        userService.createUser(user);

        model.addAttribute("users", userService.getAllUsers());

        return "redirect:/users/index";
    }

    @GetMapping("/delete/{id}")
    public String deleteBot(@PathVariable("id") String id, Model model) {
        Bot bot = botService.getBotById(Long.parseLong(id))
                .orElseThrow(() -> new IllegalArgumentException("Invalid bot Id:" + id));

        User user = userService.getUserById(bot.getUser().getId()).orElse(new User());

        List<Bot> botList = user.getBotList();

        botList.remove(bot);

        user.setBotList(botList);

        userService.createUser(user);

        model.addAttribute("users", userService.getAllUsers());

        return "redirect:/users/index";
    }
}
