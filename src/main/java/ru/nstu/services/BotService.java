package ru.nstu.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nstu.entity.Bot;
import ru.nstu.repository.BotRepository;

import java.util.Optional;

@Service
@AllArgsConstructor
public class BotService {
    private final BotRepository botRepository;
    
    public void createBot(Bot bot) {
        botRepository.save(bot);
    }

    public Iterable<Bot> getAllBots() {
        return botRepository.findAll();
    }

    public Optional<Bot> getBotById(Long id) {
        return botRepository.findById(id);
    }
    
    public void deleteBot(Bot bot) {
        botRepository.delete(bot);
    }
}
